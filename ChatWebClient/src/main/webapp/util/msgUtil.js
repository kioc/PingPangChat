//HTML转义
function HTMLEncode(str) {
	str = utf16toEntities(str);
	return filterXSS(str);
};

// 表情
function utf16toEntities(str) { // 检测utf16emoji表情 转换为实体字符以供后台存储
	var patt = /[\ud800-\udbff][\udc00-\udfff]/g;
	str = str.replace(patt, function(temp) {
		var H, L, code;
		if (temp.length === 2) { // 辅助平面字符（我们需要做处理的一类）
			H = temp.charCodeAt(0); // 取出高位
			L = temp.charCodeAt(1); // 取出低位
			code = (H - 0xD800) * 0x400 + 0x10000 + L - 0xDC00; // 转换算法
			return "&#" + code + ";";
		} else {
			return temp;
		}
	});
	return str;
}

//延时
function sleep(d){
	  for(var t = Date.now();Date.now() - t <= d;);
}

/**
 * 每条信息显示的html
 * @param id
 * @param name
 * @param msg
 * @param img
 * @param time
 * @param reply
 * @param type
 * @param isAppend
 * @returns
 */
function reply(id,name,msg,img,time,reply,type,isAppend){
	var icon = name=='系统'?'icon-40 f-red f11':'icon-120 f-green';
		var img = img==''?''+path+'/weui/images/favicon.png':img;
		var headName=name.charAt(0);
		//msg=HTMLEncode(msg);
		var html='<div class="weui-cells reply'+reply+'" id="reply'+id+'">'+
			'<div class="weui-cell">'+
				'<div class="weui-cell__hd" style="position: relative;">'+
					//'<img src="'+img+'" class="" style="width: 50px;display: block">'+
					 '<div class="head-container"><span class="head-content">'+headName+'</span></div>'+
				'</div>'+
				'<div class="weui-cell__bd">'+
					'<p>'+name+' <span class="icon '+icon+'"></span> <font>'+time+'</font></p>'+
					'<div class="message">'+msg+'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
		if(undefined==isAppend || true==isAppend){
 		if("4"==type){
 			$("#groupChat").append(html);
 		}else{
 			$("#userChat").append(html);
 		}
		}else{
			if("4"==type){
 			$("#groupChat").prepend(html);
 		}else{
 			$("#userChat").prepend(html);
 		}
		}
		
		//$(".weui-tab__panel").trigger("create");
		//自己的消息 改变头像位置
		if(reply==1){
			var cell_hd = $("#reply"+id+" .weui-cell__hd");
			var a = cell_hd.prop("outerHTML");
					cell_hd.remove();
			$("#reply"+id+" .weui-cell__bd").after(a);
		}
	}

/**
 * 历史聊天用户信息展示
 * 
 * @param toUserCode
 * @param toUserName
 * @param message
 * @returns
 */
function getOldUserCodeHmtl(toUserCode, toUserName, message) {
	if (null == message) {
		message = "...";
	}
	var name = toUserName.charAt(0);

	var oldHtml = '<div class="weui-cells demo_badge_cells" id="chat'
			+ toUserCode + '">'
			+ '<div class="weui-cell weui-cell_active" onclick=\'viewChat(\"'
			+ toUserCode + '\",\"' + toUserName + '\",\"chatFull\");\'>'
			+ '<div class="weui-cell__hd">' +
			//'<img src="${httpServletRequest.getContextPath()}/weui/images/pic_160.png">'+
			'<div class="head-container"><span class="head-content">' + name
			+ '</span></div>' + '<span class="weui-badge">New</span>'
			+ '</div>' + '<div class="weui-cell__bd">' + '<p>' + toUserName
			+ '</p>' + '<p class="demo_badge_desc" id="chatNewMsg' + toUserCode
			+ '">' + message + '</p>' + '</div>' + '</div>' + '</div>';
	return oldHtml;
}

/**
 * 在线用户、和在线群组信息展示
 * @param toUserCode
 * @param toUserName
 * @param type
 * @returns
 */
function viewOnlineUser(toUserCode, toUserName, type) {
	if ("3" == type) {
		var name = toUserName.charAt(0);
		var onlineUserHtml = '<div class="weui-cell weui-cell_active" onclick=\'viewChat(\"'
				+ toUserCode
				+ '\",\"'
				+ toUserName
				+ '\",\"chatFull\");\'>'
				+ '<div class="weui-cell__hd">'
				+ '<div class="head-container"><span class="head-content">'
				+ name
				+ '</span></div>'
				+ '</div>'
				+ '<div class="weui-cell__bd">'
				+ '<p>'
				+ toUserName
				+ '</p>'
				+ '</div>' + '</div>';
		return onlineUserHtml;
	} else if ("4" == type) {
		//var name=toUserName.charAt(0);
		var groupHtml = '<li><div class="weui-flex js-category weui-cells demo_badge_cells" id="'
				+ toUserCode
				+ '" onclick="groupChoice(\''
				+ toUserCode
				+ '\');" >'
				+ '<div class="weui-flex__item" >'
				+ toUserName
				+ '<span class="weui-badge" style="margin-left: 5px;" id="grop_'
				+ toUserCode
				+ '"></span>'
				+ '</div>'
				+ '<i class="icon icon-74"></i>'
				+ '</div>'
				+ '<div class="page-category js-categoryInner">'
				+ '<div class="weui-cells page-category-content">'
				+ '<a class="weui-cell weui-cell_access" href="javascript:void(0);" onclick=\'viewGropChat(\"'
				+ toUserCode
				+ '\",\"'
				+ toUserName
				+ '\",\"chatGroupFull\");\'>'
				+ '<div class="weui-cell__bd">'
				+ '<p style="font:15px;">进群</p>'
				+ '</div>'
				+ '<div class="weui-cell__ft"></div>'
				+ '</a>'
				+ '<a class="weui-cell weui-cell_access" href="javascript:void(0)" onclick=\'getAllUser(\"8\",\"'
				+ toUserCode
				+ '\");\'>'
				+ '<div class="weui-cell__bd">'
				+ '<p style="font:15px;">退群</p>'
				+ '</div>'
				+ '<div class="weui-cell__ft"></div>'
				+ '</a>'
				+ '</div></div></li>';

		return groupHtml;
	}
}
/**
 * 群组下拉样式
 * @param groupCode
 * @returns
 */
function groupChoice(groupCode){
	$parent =$("#"+groupCode).parent('li');
	//console.log("组："+$parent.html());
	if($parent.hasClass('js-show')){
             $parent.removeClass('js-show');
             $(this).children('i').removeClass('icon-35').addClass('icon-74');
         }else{
             $parent.siblings().removeClass('js-show');
             $parent.addClass('js-show');
             $(this).children('i').removeClass('icon-74').addClass('icon-35');
              $parent.siblings().find('i').removeClass('icon-35').addClass('icon-74');
         }
};

/**
 * 展示聊天信息弹出层的
 * @param toUserCode
 * @param toUserName
 * @param showID
 * @returns
 */
function viewChat(toUserCode,toUserName,showID){
	 //清空
	 $("#userChat").html("");
	 $("#userChatName").html(toUserName);
	 $("#userChatCode").html(toUserCode);
	 $("#chatMsg").val('');
	 currentPage=1;
	 sendMsg(toUserCode,9,"");//获取历史聊天信息
	 window.history.pushState(null, null, null);
	 $("#"+showID).popup();
	 
	 //下拉获取历史消息
    $(document.getElementById("userChat")).pullToRefresh({
        distance: 10,
        onRefresh:function() {
        console.log(currentPage++);
        sendMsg($("#userChatCode").html(),9,"");//获取历史聊天信息
        $(document.getElementById("userChat")).pullToRefreshDone();
        //return false;
    }});
}

/**
 * 展示群组聊天信息弹出层
 * @param toUserCode
 * @param toUserName
 * @param showID
 * @returns
 */
function viewGropChat(toUserCode,toUserName,showID){
	 //清空
	 $("#groupChat").html("");
	 $("#groupChatName").html(toUserName);
	 $("#groupChatCode").html(toUserCode);
	 $("#groupMsg").val('');
	 currentPage=1;
	 getAllUser("7",toUserCode);
	 sendMsg(toUserCode,9,"",true);//获取历史聊天信息
	 $("#"+showID).popup();
	 
	 $(document.getElementById("groupChat")).pullToRefresh({
        distance: 10,
        onRefresh:function() {
        console.log(currentPage++);
        sendMsg($("#groupChatCode").html(),9,"",true);//获取历史聊天信息
        $(document.getElementById("groupChat")).pullToRefreshDone();
        //return false;
    }});
}

//清空聊天信息
function closeViewChat(showID){
	 //清空
	 $("#userChat").html("");
	 $("#userChatName").html("");
	 $("#userChatCode").html("");
	 $("#chatMsg").val('');
	 //$("#"+showID).closePopup();
	 
	 $("#groupChat").html("");
	 $("#groupChatName").html("");
	 $("#groupChatCode").html("");
	 $("#groupMsg").val('');
	 currentPage=1;
}

//直接方式发送数据
function sendMsg(name, cmd, message, isGroup) {
	var bindMsg = {};
	var from = {};
	var accept = {};
	var group = {};
	var page = {};
	from.userCode = userCode;
	bindMsg.cmd = cmd;
	bindMsg.from = from;

	accept.userCode = name;
	bindMsg.accept = accept;

	if ((undefined != isGroup) || "12" == cmd) {// 直播
		group.groupCode = name;
		bindMsg.group = group;
	}
	page.page = currentPage;
	page.limit = limit;
	bindMsg.page = page;

	bindMsg.msg = HTMLEncode(message);
	// console.log("bind:"+JSON.stringify(bindMsg));
	ws.send(JSON.stringify(bindMsg));
};


/**
 * 5.获取用户信息 
 * 6.获取群组用户信息
 * 7.进群
 * 8.退群
 * @param cmd
 * @param groupCode
 * @returns
 */
function getAllUser(cmd, groupCode) {
	var bindMsg = {};
	var from = {};
	bindMsg.cmd = cmd;
	from.userCode = userCode;
	bindMsg.from = from;

	if ("7" == cmd || "8" == cmd) {
		var group = {};
		group.groupCode = groupCode;
		bindMsg.group = group;
	}
	ws.send(JSON.stringify(bindMsg));
}

/**
 * 图片文件上传 
 * @param file
 * @returns
 */
function previewImages(file) {
    var MAXWIDTH = 1000;
    var MAXHEIGHT = 1200;
    var isGroup=false;
    if($("#groupChatCode").html().length>0){
   	 isGroup=true;
    }
    
    for (var i = 0; i < file.files.length; i++) {
        if (file.files && file.files[i]) {
       	 /* lrz(file.files[i],{quality:1}).then(function(data){
       		    console.log(data.base64);
       		    $.ajax({
       		      url: '${httpServletRequest.getContextPath()}/fileUpload/imageUploadBase64',
       		      type: 'post',
       		      data: {
       		          img: data.base64
       		      },
       		      dataType: 'json',
       		      //timeout: 200000,
       		      success: function (res) {
       		    	  console.log(res);
                         if("0"==res.code){
                       	//console.log($("#userChatCode").html(),3,'<img src="'+res.data.src+'" alt="'+res.data.title+'" id="im_img">');
                           sendMsg($("#userChatCode").html(),3,'<img src="'+res.data.src+'" alt="'+res.data.title+'" id="im_img">');
                         }
       		      },
       		      error: function() {
       		      }
       		    });
       	 }); */
       	 
       	 var formData=new FormData();
       	 formData.append("file",file.files[i]);
       	 $.ajax({
                url:path+'/fileUpload/imageUpload',
                //url:path+'/fileUpload/imageUploadMinio',
                dataType:'json',
                type:'POST',
                data:formData,
                processData:false,
                contentType:false,
                success:function( res ){
                 console.log(res);
                 if("0"==res.code){
               	//console.log($("#userChatCode").html(),3,'<img src="'+res.data.src+'" alt="'+res.data.title+'" id="im_img">');
               	if(!isGroup){
                      sendMsg($("#userChatCode").html(),3,'<img src="'+res.data.src+'" alt="'+res.data.title+'" id="im_img">');
               	}else{
                      sendMsg($("#groupChatCode").html(),4,'<img src="'+res.data.src+'" alt="'+res.data.title+'" id="im_img">',true);
               	}
                 }
                }
            }); 
            var reader = new FileReader();
            reader.onload = function (evt) {
           	 if(!isGroup){
           	   reply(userCode,userName,'<img src="'+evt.target.result+'" onclick="view(this)" style="background-image:url('+evt.target.result+');background-repeat:no-repeat;background-position:center center;"height="150px" width="180px"/>','',(new Date()).Format("yyyy-MM-dd hh:mm:ss"),1);
           	 }else{
           	   reply(userCode,userName,'<img src="'+evt.target.result+'" onclick="view(this)" style="background-image:url('+evt.target.result+');background-repeat:no-repeat;background-position:center center;"height="150px" width="180px"/>','',(new Date()).Format("yyyy-MM-dd hh:mm:ss"),1,"4");
           	 //reply(userCode,userName,'<span style="width:180px;height:150px;"><li onclick="view(this)" class="weui-uploader__file" height="150px" width="180px" style="background-image:url('+evt.target.result+');background-repeat:no-repeat;background-position:center center;"></li></span>','','1月27日 11:33',1);
                }
            }
            reader.readAsDataURL(file.files[i]);
        }
    }
}
function view(img){
	 $(".weui-gallery__img").attr("style", 'background-image:url('+img.getAttribute("src")+');background-repeat:no-repeat;background-position:center center;"');
    $("#weui-gallery").fadeIn(100);
};
function hideImgShow(){
	 $("#weui-gallery").fadeOut(100);
};

/**
 * 加载图片的方法 之前的是获取图片的base64 现在改为连接了
 * 兼容之前本地存储的图片<img src="/
 * 外链的<img src="http
 * @param msg
 * @returns
 */
function loadImage(msg){
	 if(msg.startsWith('<img src="/')){
		var fileName=msg.substring(11);
		    fileName=fileName.substring(0,fileName.indexOf('"'));
		    console.log("imageName:"+fileName);
		    return '<img src=/'+fileName+' onclick="view(this)" height="150px" width="180px">';
		    
	    var base64=getImage(fileName);
	    if(""!=base64){
	    	return '<img src="'+base64+'" onclick="view(this)" height="150px" width="180px">';
	    }
	}else if(msg.startsWith('<img src="http')){
		msg=msg.slice(0,-1)+' onclick="view(this)" height="150px" width="180px">';
		return msg;
	}
	return "";
}

/**
 * 根据图片名称获取对应的base64编码
 * 图片走连接、现在已不用了
 * @param fileName
 * @returns
 */
function getImage(fileName){
	 var base64="";
	 var formData=new FormData();
	 formData.append("imageName",fileName);
	 $.ajax({
        url:path+'/fileUpload/imageBase64',
        dataType:'json',
        type:'POST',
        data:formData,
        processData:false,
        contentType:false,
        async:false,//同步
        success:function( res ){
         //console.log(res);
         if("0"==res.code){
       	    base64=res.msg;
         }
        }
    });
	 return base64;
}

/**
 * 页面滚动条
 * @returns
 */
function scrollToBottom(){
	var scrollHeight = $('.weui-tab__panel').prop("scrollHeight");
	$('.weui-tab__panel').scrollTo({toT: scrollHeight});
}
$.fn.scrollTo =function(options){
    var defaults = {
        toT : 0,    //滚动目标位置
        durTime : 100,  //过渡动画时间
        delay : 30,     //定时器时间
        callback:null   //回调函数
    };
    var opts = $.extend(defaults,options),
        timer = null,
        _this = this,
        curTop = _this.scrollTop(),//滚动条当前的位置
        subTop = opts.toT - curTop,    //滚动条目标位置和当前位置的差值
        index = 0,
        dur = Math.round(opts.durTime / opts.delay),
        smoothScroll = function(t){
            index++;
            var per = Math.round(subTop/dur);
            if(index >= dur){
                _this.scrollTop(t);
                window.clearInterval(timer);
                if(opts.callback && typeof opts.callback == 'function'){
                    opts.callback();
                }
                return;
            }else{
                _this.scrollTop(curTop + index*per);
            }
        };
    timer = window.setInterval(function(){
        smoothScroll(opts.toT);
    }, opts.delay);
    return _this;
};